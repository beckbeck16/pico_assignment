SELECT DISTINCT a.user_id, a.social_network_id, b.value, c.name AS data_type
FROM b 
INNER JOIN a ON b.user_id = a.user_id
INNER JOIN c ON b.data_id = c.data_id;



SELECT a.social_network_id, a.user_id, firstname.first_name, lastname.last_name, gender.gender, email.email, phonenumber.phone_number, uw.number_of_widgets
FROM (SELECT d.user_id, COUNT(*)
	  FROM d
	  GROUP BY d.user_id) as uw
INNER JOIN a ON a.user_id = uw.user_id 
INNER JOIN (SELECT user_id, b.value AS first_name
			FROM b 
			INNER JOIN c ON b.data_id = c.data_id
			WHERE c.name = "First name") AS firstname 
			ON a.user_id = firstname.user_id 
INNER JOIN (SELECT user_id, b.value AS last_name
			FROM b 
			INNER JOIN c ON b.data_id = c.data_id
			WHERE c.name = "Last name") AS lastname 
			ON a.user_id = lastname.user_id 
INNER JOIN (SELECT user_id, b.value AS gender
			FROM b 
			INNER JOIN c ON b.data_id = c.data_id
			WHERE c.name = "Gender") AS gender 
			ON a.user_id = gender.user_id 
INNER JOIN (SELECT user_id, b.value AS email
			FROM b 
			INNER JOIN c ON b.data_id = c.data_id
			WHERE c.name = "Email") AS email 
			ON a.user_id = email.user_id 
INNER JOIN (SELECT user_id, b.value AS phone_number
			FROM b 
			INNER JOIN c ON b.data_id = c.data_id
			WHERE c.name = "Phone Number") AS phonenumber 
			ON a.user_id = phonenumber.user_id;