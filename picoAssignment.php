<?php

class PicoObject
{
    // Facebook API
    // curl -X POST -H "Content-Type: application/json" -d '{
    //     "messaging_type": "<MESSAGING_TYPE>",
    //     "recipient": {
    //       "id": "<PSID>"
    //     },
    //     "message": {
    //       "text": "hello, world!"
    //     }
    //   }' "https://graph.facebook.com/v12.0/me/messages?access_token=<PAGE_ACCESS_TOKEN>"

    public ?string $messaging_type;
    public $recipients = []; // data will be from recipient.id
    public ?string $message; // data will be from message.text

    $myDBClass = new myDBClass();
    $myCache = new myCache();
    $myAIAnalysis = new myAIAnalysis();
    $myWebSocket = new myWebSocket();

    public function __construct(?string $messaging_type, $recipients, ?string $message)
    {
        $this->messaging_type = $messaging_type;
        $this->recipients = $recipients;
        $this->message = $message;
    }

    public function saveInDB()
    {
        echo 'Save to db';
        $myDBClass->save();
    }

    public function saveInCache()
    {
        echo 'Save in cache';
        $myCache->save();
    }

    public function analysis()
    {
        echo 'Analyse';
        $myAIAnalysis->analyse();
    }

    public function updateWS()
    {
        echo 'Update web socket';
        $myWebSocket->update();
    }
}

class messageHandler
{
    public function handleMessage()
    {
        echo 'handle';
    }

    public function translateMessage()
    {

    }
}

class FBMessengerHandler extends messageHandler
{
    public function handleMessage($jsonString)
    {
        echo 'handle Facebook message';
        ?PicoObject $currObject = $this->translateMessage($jsonString);
        $currObject->saveInDB();
        $currObject->saveInCache();
        $currObject->analysis();
        $currObject->updateWS();
    }
    
    public function translateMessage()
    {
        ?PicoObject $currObject = new PicoObject();
        $currObject = json_decode($jsonString); // I need to map between json structure to my object structure        
        return $currObject;
    }
}

?FBMessengerHandler $FBMessengerHandler = new FBMessengerHandler();
$FBMessengerHandler->handleMessage($jsonString);

?>